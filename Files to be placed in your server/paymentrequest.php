<?php 

$salt = '5dfc1367250d144ab36d4fc6e79d4dd23eec2d5c'; //Pass your SALT here
$_POST['api_key'] = '0fe08075-b076-4501-acc3-d6212894b3ef'; //Pass your API KEY here
                     

$hash = hashCalculate($salt, $_POST);

function hashCalculate($salt,$input){
	/* Columns used for hash calculation, Donot add or remove values from $hash_columns array */
	$hash_columns = ['address_line_1', 'address_line_2', 'amount', 'api_key', 'city', 'country', 'currency', 'description', 'email', 'mode', 'name', 'order_id', 'phone', 'return_url', 'state', 'udf1', 'udf2', 'udf3', 'udf4', 'udf5', 'zip_code','bank_code','payer_virtual_address','card_number','expiry_date','cvv','card_holder_name'];
	/*Sort the array before hashing*/
	sort($hash_columns);

	/*Create a | (pipe) separated string of all the $input values which are available in $hash_columns*/
	$hash_data = $salt;
	foreach ($hash_columns as $column) {
		if (isset($input[$column])) {
			if (strlen($input[$column]) > 0) {
				$hash_data .= '|' . trim($input[$column]);
			}
		}
	}
	$hash = strtoupper(hash("sha512", $hash_data));
	
	return $hash;
}

?>


<p>Redirecting...</p>
<form action="https://biz.aggrepaypayments.com/v2/paymentseamlessrequest" id="payment_form" method="POST">
<input type="hidden" value="<?php echo $hash; ?>"                   name="hash"/>
<input type="hidden" value="<?php echo $_POST['api_key'];?>"        name="api_key"/>
<input type="hidden" value="<?php echo $_POST['return_url']; ?>"    name="return_url"/>
<input type="hidden" value="<?php echo $_POST['mode'];?>"           name="mode"/>
<input type="hidden" value="<?php echo $_POST['order_id'];?>"       name="order_id"/>
<input type="hidden" value="<?php echo $_POST['amount'];?>"         name="amount"/>
<input type="hidden" value="<?php echo $_POST['currency'];?>"       name="currency"/>
<input type="hidden" value="<?php echo $_POST['description'];?>"    name="description"/>
<input type="hidden" value="<?php echo $_POST['name'];?>"           name="name"/>
<input type="hidden" value="<?php echo $_POST['email'];?>"          name="email"/>
<input type="hidden" value="<?php echo $_POST['phone'];?>"          name="phone"/>
<input type="hidden" value="<?php echo $_POST['address_line_1'];?>" name="address_line_1"/>
<input type="hidden" value="<?php echo $_POST['address_line_2'];?>" name="address_line_2"/>
<input type="hidden" value="<?php echo $_POST['city'];?>"           name="city"/>
<input type="hidden" value="<?php echo $_POST['state'];?>"          name="state"/>
<input type="hidden" value="<?php echo $_POST['zip_code'];?>"       name="zip_code"/>
<input type="hidden" value="<?php echo $_POST['country'];?>"        name="country"/>
<input type="hidden" value="<?php echo $_POST['udf1'];?>"           name="udf1"/>
<input type="hidden" value="<?php echo $_POST['udf2'];?>"           name="udf2"/>
<input type="hidden" value="<?php echo $_POST['udf3'];?>"           name="udf3"/>
<input type="hidden" value="<?php echo $_POST['udf4'];?>"           name="udf4"/>
<input type="hidden" value="<?php echo $_POST['udf5'];?>"           name="udf5"/>
<input type="hidden" value="<?php echo $_POST['card_number'];?>"    name="card_number"/>
<input type="hidden" value="<?php echo $_POST['expiry_date'];?>"    name="expiry_date"/>
<input type="hidden" value="<?php echo $_POST['cvv'];?>"            name="cvv"/>
<input type="hidden" value="<?php echo $_POST['card_holder_name'];?>"name="card_holder_name"/>
<input type="hidden" value="<?php echo $_POST['bank_code'];?>"      name="bank_code"/>
<input type="hidden" value="<?php echo $_POST['payer_virtual_address'];?>" name="payer_virtual_address"/>


<noscript><input type="submit" value="Continue"/></noscript>
</form>


<script>

function formAutoSubmit () {
	var payform = document.getElementById("payment_form");
	payform.submit();
}
window.onload = formAutoSubmit;

</script>
