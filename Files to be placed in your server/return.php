<?php

$salt = '5dfc1367250d144ab36d4fc6e79d4dd23eec2d5c'; //Pass your SALT here


function responseHashCheck($salt, $response_array)
{
	$response_hash = $response_array['hash'];
	unset($response_array['hash']);
	$calculated_hash = hashCalculate($salt, $response_array);
	return ($response_hash == $calculated_hash) ? true : false;
}


function hashCalculate($salt, $input)
{
	$hash_columns = array_keys($input);
	sort($hash_columns);
	$hash_data = $salt;
	foreach ($hash_columns as $column) {
		if (isset($input[$column])) {
			if (strlen($input[$column]) > 0) {
			$hash_data .= '|' . trim($input[$column]);
			}
		}
	}
	$hash = strtoupper(hash("sha512", $hash_data));
	return $hash;
 
}


if(isset($_POST['response_code']))
{	

	if(hashCalculate($salt, $_POST))
	{
		if($_POST['response_code'] == 0)
		{  
		
		} 													   
		else
		{

		}
	}
	else
	{
		
	}
}
?>

<script>

window.addEventListener("flutterInAppWebViewPlatformReady", function(event) {

 const args = ["<?php echo $_POST['response_code']?>",];
 window.flutter_inappwebview.callHandler('transactionReturn', ...args);

});
	

</script>

