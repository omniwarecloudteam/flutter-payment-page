Flutter payment page sdk ReadMe






Server-side setup

In the "Files to be placed in your server" folder, you would find 4 files -

paymentRequest.php
return.php
upiintentrequest.php
getCardInfo.php

1.Place the above files in your server.
  These are the files through which you would be querying to
  make transactions.
  
2.Enter your API key and salt given to you in the [paymentRequest.php,
  return.php, upiintentrequest.php] at places wherever prompted.
  
3.The response for all the payment methods would be posted to the
  return.php file, whereas the response for the upi intent payment would
  be returned to the upiIntentRequest.php. You could build infrastructure
  code around it to take advantage of the response.
  
  
  
  
  



Using the payment_page_sdk

To invoke the payment page, call the displayPaymentPage function 
(in the onPressed section of a button)

implementation:
displayPaymentPage(BuildContext context,
				   PaymentReq paymentRequest,
				   List<String> netbankingBankCodes,
				   List<String> walletBankCodes);


Parameters -

BuildContext ctx

// give in the list of enabled bank codes
List<String> netbankingBankCodes = ['HDFN','ICIN','SBIN']
List<String> walletBankCodes = ['MBKW','PTMW','PHPW']

PaymentReq paymentRequest = new PaymentReq(
	
	upiIntentRequestUrl: Link to the upiIntentRequest.php on your server,
	paymentRequestUrl: Link to paymentRequest.php on your server,
	cardInfoRequestUrl: Link to getCardInfo.php on your server,
	returnUrl: Link to return.php on your server,
	orderId:,
	mode:,
	currency:,
	amount:,
	name:,
	phone: ,
	email:,
	description:,
	city:,
	state:,
	country:,
	zipCode:,
	addressLine1:,
	addressLine2:,
	udf1:,
	udf2:,
	udf3:,
	udf4:,
	udf5:,

)

Note:
upiIntentRequestUrl,paymentRequestUrl,cardInfoRequestUrl,returnUrl
orderId,amount,name,phone,email,description,city,state,country,zipCode,
are all required arguments for creating the PaymentReq Object.

mode,currency,addressLine1,addressLine2,udf1,udf2,udf3,udf4,udf5
are all optional parameters

By default, mode is given the value LIVE and currency is given the value INR
