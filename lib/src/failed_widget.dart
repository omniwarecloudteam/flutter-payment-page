import 'package:flutter/material.dart';
import 'size_config.dart';

import 'theme_data.dart';

class FailedWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Theme(
      data:basicTheme,
      child: Container(
          padding: const EdgeInsets.all(25),
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.warning_amber,
                    size: proportionateSize(25),
                    color: Color(0xffe60000),
                  ),
                  SizedBox(width:proportionateWidth(10)),
                  Text(
                    'Transaction Failed',
                    style:Theme.of(context).textTheme.headline1!.copyWith(
                      color: Color(0xffe60000),
                      fontSize: proportionateWidth(23),
                      fontWeight: FontWeight.normal,
                    ), 
                  ),
                ],
              ),
              SizedBox(height: proportionateHeight(5),),
              Text(
                'If any money was debited, it will be refunded within 4-5 days',
                style:Theme.of(context).textTheme.bodyText1, 
              ),
    
              SizedBox(height:proportionateHeight(30)),
    
              ElevatedButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child: Text('Try Again'),
              ),
              SizedBox(height: proportionateHeight(10),),
              OutlinedButton(
                onPressed: (){
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
                child: Text('Cancel')
              )
    
    
    
    
            ],
          ),
    
      ),
    );
  }
}