import 'dart:convert';

import 'dart:io' as io;
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

import 'payment_req.dart';
import 'success_page.dart';
import 'failed_widget.dart';

class WebviewPage extends StatefulWidget {

  static final routeName = '/WebviewPage'; 

  final PaymentReq paymentrequest;
  final Map<String,Map<String,String>>? paymentChannelArgs;
  WebviewPage(this.paymentChannelArgs,this.paymentrequest);

  @override
   WebviewPageState createState() => new  WebviewPageState();
}

class  WebviewPageState extends State <WebviewPage> {

  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  late PullToRefreshController pullToRefreshController;
  String url = "";
  double progress = 0;
  final urlController = TextEditingController();

  @override
  void initState() {

    super.initState();

    WidgetsFlutterBinding.ensureInitialized();

    if (io.Platform.isAndroid) {
       AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }

    
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Theme.of(context).primaryColor,
      ),
      onRefresh: () async {
        if (io.Platform.isAndroid) {
          webViewController?.reload();
        } else if (io.Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );


    Map<String,Map<String,String>>? args = widget.paymentChannelArgs;
    PaymentReq payreq = widget.paymentrequest;
    // var args = ModalRoute.of(context)!.settings.arguments as Map<String,Map<String,String>>;
    var postBody;

    String cardHolderName="";
    String cardNumber="";
    String cvv="";
    String expiryDate="";
    String bankCode="";
    String payerVirtualAddress="";

    if(args != null)
    {
      if(args['cards'] != null)
      {
        cardHolderName = args['cards']!['cardHolderName'] as String;
        cardNumber = args['cards']!['cardNumber'] as String;
        cvv = args['cards']!['cvv'] as String;
        expiryDate = args['cards']!['expiryDate'] as String;
        bankCode = args['cards']!['bankCode'] as String;

        postBody = "card_holder_name="+cardHolderName+"&"+
                  "card_number="+cardNumber+"&"+
                  "expiry_date="+expiryDate+"&"+
                  "cvv="+cvv+"&"+
                  "payer_virtual_address=0&"+
                  "bank_code="+bankCode+"&";
      }

      if(args['netbanking'] != null)
      {
        bankCode = args['netbanking']!['bankCode'] as String;

        postBody = "card_holder_name=0&"+
                  "card_number=0&"+
                  "expiry_date=0&"+
                  "cvv=0&"+
                  "payer_virtual_address=0&"+
                  "bank_code="+bankCode+"&";
      }
      
      if(args['wallet'] != null)
      {
        bankCode = args['wallet']!['bankCode'] as String;

        postBody = "card_holder_name=0&"+
                  "card_number=0&"+
                  "expiry_date=0&"+
                  "cvv=0&"+
                  "payer_virtual_address=0&"+
                  "bank_code="+bankCode+"&";
      }

      if(args['upi'] != null)
      {
        bankCode = args['upi']!['bankCode'] as String;
        payerVirtualAddress = args['upi']!['payerVirtualAddress'] as String;

        postBody = "card_holder_name=0&"+
                  "card_number=0&"+
                  "expiry_date=0&"+
                  "cvv=0&"+
                  "payer_virtual_address="+payerVirtualAddress+"&"+
                  "bank_code="+bankCode+"&";
      }

    }
    
/*
    postBody += "return_url=https://uatpgbiz.omniware.in/pay-modal-cdn/test/return.php&"+
                "order_id="+random.nextInt(1000000).toString()+"&"
                "mode=LIVE&"+
                "amount=2&"+
                "currency=INR&"+
                "name=Peter&"+
                "phone=9886677889&"+
                "email=peter@gmail.com&"+
                "address_line_2=address2&"+
                "address_line_1=address1&"+
                "city=city&"+
                "state=state&"+
                "zip_code=560073&"+
                "country=India&"+
                "description=itemdesc&"+
                "udf1=udf1&"+
                "udf2=udf2&"+
                "udf3=udf3&"+
                "udf4=udf4&"+
                "udf5=udf5&"; */


    postBody += "return_url="+payreq.returnUrl+"&"+
                "order_id="+payreq.orderId+"&"
                "mode="+payreq.mode+"&"+
                "amount="+payreq.amount+"&"+
                "currency="+payreq.currency+"&"+
                "name="+payreq.name+"&"+
                "phone="+payreq.phone+"&"+
                "email="+payreq.email+"&"+
                "address_line_1="+payreq.addressLine1.toString()+"&"+
                "address_line_2="+payreq.addressLine2.toString()+"&"+
                "city="+payreq.city+"&"+
                "state="+payreq.state+"&"+
                "zip_code="+payreq.zipCode+"&"+
                "country="+payreq.country+"&"+
                "description="+payreq.description+"&"+
                "udf1="+payreq.udf1.toString()+"&"+
                "udf2="+payreq.udf2.toString()+"&"+
                "udf3="+payreq.udf3.toString()+"&"+
                "udf4="+payreq.udf4.toString()+"&"+
                "udf5="+payreq.udf5.toString()+"&";



    return MaterialApp(
      home: Scaffold(
          
          body: SafeArea(
              child: Column(children: <Widget>[

                Expanded(
                  child: Stack(
                    children: [
                      InAppWebView(
                        key: webViewKey,
                        initialUrlRequest:URLRequest(
                          url: Uri.parse(payreq.paymentRequestUrl),
                          method: "POST",
                          body: Uint8List.fromList(utf8.encode(postBody)),
                        ),
                        initialOptions: options,
                        pullToRefreshController: pullToRefreshController,
                        onWebViewCreated: (controller) {
                          webViewController = controller;
                          webViewController!.addJavaScriptHandler(
                            handlerName: 'transactionReturn',
                            callback: (args){
                                if(args[0] == '0')
                                {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).popAndPushNamed(SuccessPage.routeName);
                                }
                                else
                                {
                                  Navigator.of(context).pop();
                                  showModalBottomSheet(
                                    context: context,
                                    isScrollControlled: true,
                                    builder: (_){
                                        return SingleChildScrollView(
                                          child: FailedWidget(),
                                        );
                                    },
                                  );
                                }
                                
                            },
                          );

                        },
                        onLoadStart: (controller, url) {
                          setState(() {
                            this.url = url.toString();
                            urlController.text = this.url;
                          });
                        },
                        androidOnPermissionRequest: (controller, origin, resources) async {
                          return PermissionRequestResponse(
                              resources: resources,
                              action: PermissionRequestResponseAction.GRANT);
                        },
                        shouldOverrideUrlLoading: (controller, navigationAction) async {
                          var uri = navigationAction.request.url!;

                          if (![ "http", "https", "file", "chrome",
                            "data", "javascript", "about"].contains(uri.scheme)) {
                            if (await canLaunch(url)) {
                              // Launch the App
                              await launch(
                                url,
                              );
                              // and cancel the request
                              return NavigationActionPolicy.CANCEL;
                            }
                          }

                          return NavigationActionPolicy.ALLOW;
                        },
                        onLoadStop: (controller, url) async {
                          pullToRefreshController.endRefreshing();
                          
                        },
                        onLoadError: (controller, url, code, message) {
                          pullToRefreshController.endRefreshing();
                        },
                        onProgressChanged: (controller, progress) {
                          if (progress == 100) {
                            pullToRefreshController.endRefreshing();
                          }
                          setState(() {
                            this.progress = progress / 100;
                            urlController.text = this.url;
                          });
                        },
                        
                      ),
                      progress < 1.0
                          ? LinearProgressIndicator(
                              value: progress,
                              color: Theme.of(context).primaryColor,
                              backgroundColor: Theme.of(context).primaryColorLight,)
                          : Container(),
                    ],
                  ),
                ),
                
              ]
            )
          )
        ),
    );
  }
}
