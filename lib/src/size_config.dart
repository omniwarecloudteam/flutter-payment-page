import 'package:flutter/widgets.dart';

class SizeConfig {

  static var _mediaQueryData;
  static double deviceWidth = 0;
  static double deviceHeight = 0;
  
  void init(BuildContext context) {

    _mediaQueryData = MediaQuery.of(context);
    deviceWidth = _mediaQueryData.size.width;
    deviceHeight = _mediaQueryData.size.height;
    
  }

}

double proportionateHeight(double inputHeight)
{
 
  double screenHeight = SizeConfig.deviceHeight;
  return (inputHeight/803.63)* screenHeight;
}

proportionateWidth(double inputWidth)
{
   
  double screenWidth = SizeConfig.deviceWidth; 
  return (inputWidth/392.72)*screenWidth;
}

proportionateSize(double inputSize)
{
  double screenHeight = SizeConfig.deviceHeight;
  double screenWidth = SizeConfig.deviceWidth; 
  return (inputSize/(803.63/392.72))*(screenHeight/screenWidth);
}