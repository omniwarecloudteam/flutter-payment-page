import 'package:flutter/material.dart';


import 'size_config.dart';
import 'payment_widgets/cash_on_delivery_widget.dart';
import 'payment_widgets/card_widget.dart';
import 'payment_widgets/netbanking_widget.dart';
import 'payment_widgets/upi_widget.dart';
import 'payment_widgets/wallet_widget.dart';
import 'theme_data.dart';
import 'payment_req.dart';


class PaymentPage extends StatelessWidget {

  final PaymentReq paymentRequest;
  final List<String> netbankingCodesList;
  final List<String> walletCodesList;

  PaymentPage({
    required this.paymentRequest,
    required this.netbankingCodesList,
    required this.walletCodesList,
  });


 
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget placeSpacingContainer(){
      return Container(
        height: proportionateHeight(10),
        color: Color(0xffededed),  
      );
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Payment Options',style:TextStyle(
              color: Color(0xff4a4a4a),
              fontSize: proportionateWidth(20),
              fontWeight: FontWeight.w300
            ),
          ),
        ),
        body: SafeArea(
          child: Theme(
            data: basicTheme,
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
              
                    placeSpacingContainer(),
                    CashOnDeliveryWidget(paymentrequest:paymentRequest),
                    placeSpacingContainer(),
                    UpiWidget(paymentrequest:paymentRequest),
                    placeSpacingContainer(),
                    NetbankingWidget(paymentrequest:paymentRequest,bankCodesList:netbankingCodesList),
                    placeSpacingContainer(),
                    WalletWidget(paymentrequest:paymentRequest,bankCodesList:walletCodesList),
                    placeSpacingContainer(),
                    CardWidget(paymentrequest:paymentRequest),
                    placeSpacingContainer(),
            
                  ],
              ),
            ),
          ),
        ),
    
      );
  }
}

