List<Map<String,String>> walletTiles = [
  {
    "bankCode":"AMPW",
    "title":"Amazon",
    "image":"amazon pay",
    "height":"50",
    "width":"60",
  },
  {
    "bankCode":"PHPW",
    "title":"PhonePe",
    "image":"phonepe",
    "height":"50",
    "width":"52",
  },

  {
    "bankCode":"PTMW",
    "title":"PayTM",
    "image":"paytm",
    "height":"50",
    "width":"100",
  },
  
  {
    "bankCode":"MBKW",
    "title":"Mobikwik",
    "image":"mobikwik",
    "height":"50",
    "width":"87",
  },
  
  {
    "bankCode":"ATLW",
    "title":"Airtel",
    "image":"airtel",
    "height":"50",
    "width":"48",
  },
  {
    "bankCode":"FRCW",
    "title":"Freecharge",
    "image":"freecharge",
    "height":"50",
    "width":"50",
  },

];


var walletOptionsMapper = 

{
	'MBKW':'mobikwik',
		
	'PHPW':'phonepe',
			
	'FRCW':'freecharge',
			
	'PZAW':'payzapp',
			
	'AMPW':'amazon pay',
			
	'PTMW':'paytm',
			
	'OLAW':'ola',
			
	'ATLW':'airtel',
			
	'JIOW':'jio',
			
	'YESW':'Yes pay',
			
	'ZIPW':'Zipcash',
			
	'YPAW':'Ypay cash ',
			
	'CITW':'Citibank',
			
	'IDMW':'Idea Money',
			
	'BKSW':'bKash',
			
	'DCBW':'DCB Cippy',
		
	'EZCW':'ezeClick',
			
	'MRPW':'mRupee',
			
	'PYCW':'Paycash',
		
	'PNBW':'PNB Wallet',
		
	'OXIW':'Oxigen',
		
	'JNCW':'Jana Cash',
			
	'TMWW':'The Mobile Wallet',
		
	'VDFW':'mPesa Wallet',
		
};