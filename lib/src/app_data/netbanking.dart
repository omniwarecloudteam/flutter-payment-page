List<Map<String,String>> bankTiles = [
  
  {
    "bankCode":"HDFN",
    "title":"HDFC",
    "image":"hdfc",
    "height":"42",
    "width":"42",
  },
  {
    "bankCode":"ICIN",
    "title":"ICICI",
    "image":"icici",
    "height":"42",
    "width":"42",
  },

  {
    "bankCode":"SBIN",
    "title":"SBI",
    "image":"sbi",
    "height":"42",
    "width":"42",
  },
  {
    "bankCode":"KKBN",
    "title":"Kotak",
    "image":"kotak",
    "height":"42",
    "width":"42",
  },
  {
    "bankCode":"YESN",
    "title":"Yes",
    "image":"yes",
    "height":"42",
    "width":"48",
  },
  {
    "bankCode":"AXIN",
    "title":"Axis",
    "image":"axis",
    "height":"42",
    "width":"42",
  },

];




var netbankingOptionsMapper = 

{
	'ALLN':'Allahabad Bank (Erstwhile Indian Bank)',
		
	'ALLM':'allahabad bank - corporate',
		
	'BRLM':'barclays corporate banking',
	
	'ABPN':'aditya birla payments bank',
		
			
	'ZOBN':'zoroastrian co-op bank',
	
	'HDFN':'hdfc',
	
	'SBIN':'sbi',
	
	'AXIN':'axis',
	
	'ICIN':'icici',
	
	'ICIM':'icici - corporate',
			
	'YESN':'yes',
		
	'YESM':'Yes - Corporate',
		
	'IDBM':'IDBI - Corporate',

	'KKBN':'kotak',
	
	'BBRN':'bank of baroda',
		
	'SBMN':'state bank of mysore',
		
	'SBTN':'state bank of travancore',
		
	'SBPN':'state bank of patiala',
		
	'SBHN':'state bank of hyderabad',
		
	'SBJN':'state bank of bikaner and jaipur',
		
	'BMBN':'bharatiya mahila bank',
		
	'ESAN':'ESAF Small Finance Bank',
		
	'ADCN':'Ahmedabad District Co-op Bank',
		
	'NEBN':'North East Small Finance Bank',
			
	'VRBN':'Varachha Co-op Bank',
		
	'SUBN':'Suryoday Small Finance Bank',
		
	'FESN':'Federal Bank Scan And Pay',
			
	'AUSN':'AU Small Finance Bank',
			
	'GPPN':'GP Parsik Sahakari Bank',
				
	'NBLN':'Nainital Bank',
			
	'BHAN':'Bharat Bank',
			
	'AIRN':'Airtel Payment Bank',
			
	'KCCN':'Kalupur Commercial Co-op Bank',
			
	'PNYN':'PNB YUVA Bank',
			
	'ESFN':'Equitas Bank',
			
	'VIJN':'Vijaya Bank',
		
	'UCON':'UCO Bank',
			
	'UBIN':'Union Bank of India',
			
	'UBIM':'Union Bank of India - Corporate',
			
	'TSCN':'Tamilnadu State Co-op Bank',
			
	'UNIN':'PNB (Erstwhile-United Bank of India)',
			
	'TMBN':'Tamilnad Mercantile Bank',
			
	'TJSN':'TJSB Bank',
			
	'SYDN':'Syndicate Bank (Erstwhile Canara Bank)',
		
	'SVCN':'Shamrao Vitthal co-op Bank',
			
	'SVCM':'Shamrao Vitthal co-op Bank - Corporate',
			
	'SRSN':'Saraswat Bank',
			
	'SOIN':'South Indian Bank',
			
	'SCBN':'Standard Chartered Bank',
			
	'RTNN':'RBL Bank',
			
	'RTNM':'RBL Bank - corporate',
			
	'PSBN':'Punjab & Sind Bank',
			
	'PNBN':'PNB',
		
	'PNBM':'PNB - Corporate',
			
	'PMCN':'Punjab & Maharashtra Co-op Bank',
			
	'OBCN':'PNB (Erstwhile-Oriental Bank of Commerce)',
			
	'NKBN':'NKGSB Bank',
			
	'MSBN':'Mehsana Urban co-op Bank',
			
	'LVBN':'Laxmi Vilas bank',
			
	'LVBM':'Laxmi Vilas bank - corporate',
			
	'KRVN':'Karur Vysya bank',
			
	'KRVM':'Karur Vysya - Corporate',
			
	'KJBN':'Kalyan Janata Sahakari Bank',
			
	'JSBN':'Janata Sahakari Bank',
		
	'JAKN':'Jammu and Kashmir bank',
			
	'IOBN':'Indian Overseas bank',
			
	'ININ':'Indian bank',
			
	'INGN':'ING Vysya Bank',
			
	'INDN':'IndusInd bank',
			
	'IDFN':'IDFC bank',
			
	'IDBN':'IDBI bank',
			
	'FEDN':'Federal Bank',
			
	'DSHN':'Deutsche Bank',
		
	'DHNN':'Dhanlaxmi Bank',
			
	'DHNM':'Dhanlaxmi Bank - Corporate',
		
	'DENN':'Dena Bank',
			
	'DCBN':'DCB Bank',
		
	'DCBM':'DCB Bank - Corporate',
		
	'DBSN':'DBS bank',
			
	'CUBN':'City Union Bank',
			
	'CSBN':'Catholic syrian Bank',
			
	'COSN':'Cosmos Bank',
		
	'CITN':'CitiBank',
		
	'CBIN':'Central bank of india',
			
	'CANN':'Canara Bank',
		
	'BOMN':'Bank of Maharashtra',
			
	'BOIN':'Bank of India',
			
	'BDNN':'Bandhan Bank',
		
	'BCBN':'Bassein Catholic Bank',
			
	'BBRM':'Bank of Baroda - Corporate',
			
	'BBKN':'Bank of Bahrain and Kuwait',
			
	'ADBN':'Union Bank of India (Erstwhile Andhra Bank)',
		
	'ADBM':'Union Bank of India (Erstwhile Andhra Bank) - Corporate',
	
};



