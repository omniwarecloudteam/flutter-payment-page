import 'package:flutter/material.dart';

import 'size_config.dart';
import 'theme_data.dart';


class SuccessPage extends StatelessWidget {

  static String routeName = '/SuccessPage';

  @override
  Widget build(BuildContext context) {
    
    
    double deviceHeight = MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;
    
    return Scaffold(

      body: Theme(
        data: basicTheme,
        child: SafeArea(
          top: false,
          child: Container(
            
            child:Column(
              children: [
                Container(
                  height: deviceHeight*0.4,
                  width: double.infinity,
                  color: Color(0xff04b46c),
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      
                      Text('Payment',
                            style:TextStyle(
                              fontSize: 23,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                      ),
                      SizedBox(height: proportionateHeight(30),),
                      Icon(Icons.check_circle_sharp,size: proportionateSize(60),color: Colors.white,),
                      SizedBox(height: proportionateHeight(30),),
                      Text('Paid \u{20B9} '+''+'Successfully',
                            style:TextStyle(
                              fontSize: 23,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            )
                      ),
                    ],
                  )
                  
                ),
                SizedBox(height:proportionateHeight(130)),
                ElevatedButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  child: Text('Continue'),
                  style: ElevatedButton.styleFrom(
                    
                    fixedSize: Size(deviceWidth*0.5,proportionateHeight(50)),
                    minimumSize: Size(deviceWidth *0.5,proportionateHeight(50)),
                  )
                ),
              ],
            ),
          
              
          ),
        ),
      ),
      


    );
  }
}






