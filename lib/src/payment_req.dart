class PaymentReq{

  String upiIntentRequestUrl;
  String paymentRequestUrl;
  String cardInfoRequestUrl;
  String returnUrl;
  var orderId;
  String mode = 'LIVE';
  var amount;
  String currency = 'INR';
  String name = '';
  var phone;
  String email='';
  String? addressLine1 ='';
  String? addressLine2 ='';
  String city='';
  String state ='';
  var zipCode;
  String country='';
  String description='';
  String? udf1;
  String? udf2;
  String? udf3;
  String? udf4;
  String? udf5;

  PaymentReq({
    required this.upiIntentRequestUrl,
    required this.paymentRequestUrl,
    required this.cardInfoRequestUrl,
    required this.returnUrl,
    required this.orderId,
    this.currency = 'INR',
    this.mode = 'LIVE',
    required this.amount,
    required this.name,
    required this.phone,
    required this.email,
    required this.description,
    required this.city,
    required this.state,
    required this.country,
    required this.zipCode,
    this.addressLine1 ="none",
    this.addressLine2 ="none",
    this.udf1 ="none",
    this.udf2 ="none",
    this.udf3 ="none",
    this.udf4 ="none",
    this.udf5 ="none",
   
  });

}


