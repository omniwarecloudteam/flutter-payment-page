import 'package:flutter/material.dart';

import 'size_config.dart';

ThemeData basicTheme =  ThemeData(
        
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            side: BorderSide(width: 1, color: Color(0xffe1003d)),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(60)
            ),
            onPrimary: Colors.white,
            primary: Color(0xffe1003d),
            elevation: 0,
            alignment: Alignment.center,
            fixedSize: Size(double.infinity,proportionateHeight(50)),
            minimumSize: Size(double.infinity,proportionateHeight(50)),
            textStyle: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
          )
        ),
        primaryColor: Color(0xffe1003d),
        primarySwatch: const MaterialColor(0xffe1003d,const{
          50:Color(0xffF07F9E),
          100:Color(0xffED668A),
          200:Color(0xffEA4C77),
          300:Color(0xffE73363),
          400:Color(0xffE41950),
          500:Color(0xffE1003D),
          600:Color(0xffCA0036),
          700:Color(0xffB40030),
          800:Color(0xff9D002A),
          900:Color(0xff870024),
        }),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(60)
            ),
            primary: Color(0xff4a4a4a),
            fixedSize: Size(double.infinity,proportionateHeight(50)),
            minimumSize: Size(double.infinity,proportionateHeight(50)),
            
            side: BorderSide(width: 1, color: Color(0xff4a4a4a)),
            textStyle: TextStyle(fontSize: 17,color: Color(0xff4a4a4a),fontWeight: FontWeight.bold),
            alignment: Alignment.center,

          )
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Color(0xffe1003d),
        ),
        inputDecorationTheme: InputDecorationTheme(
          errorStyle: TextStyle(fontSize: 13),
          labelStyle: TextStyle(fontSize: 13,),
          contentPadding: EdgeInsets.symmetric(horizontal: 8,vertical:4),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffe1003d),
            )
          ),
          enabledBorder: OutlineInputBorder(),
        ),

        
        textTheme: TextTheme(
          
          bodyText1: TextStyle(
            color: Color(0xff4a4a4a),
            fontSize: proportionateWidth(16),
          ),
          headline1: TextStyle(
            color: Color(0xff4a4a4a),
            fontSize: proportionateWidth(20),
            fontWeight: FontWeight.w300
          ),
          headline2: TextStyle(
            color: Color(0xff4a4a4a),
            fontSize: proportionateWidth(18),
          ),
          subtitle1: TextStyle(
            color: Color(0xff4a4a4a),
            fontSize: proportionateWidth(18),
          ),
          caption: TextStyle(
            color: Color(0xff4a4a4a),
            fontSize: proportionateWidth(13),
          ),
          
        ),
        
      );