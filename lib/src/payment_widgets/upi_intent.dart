import 'dart:io';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:upi_pay/upi_pay.dart';
import '../payment_req.dart';


class UpiIntent extends StatefulWidget {

  final PaymentReq paymentrequest;
  UpiIntent(this.paymentrequest);

  @override
  UpiIntentState createState() => UpiIntentState();
}

class UpiIntentState extends State<UpiIntent> {

  List<ApplicationMeta>? _apps;

  late PaymentReq payreq;

  @override
  void initState() {
    super.initState();

    payreq = widget.paymentrequest;

    Future.delayed(Duration(milliseconds: 0), () async {
      _apps = await UpiPay.getInstalledUpiApplications(
          statusType: UpiApplicationDiscoveryAppStatusType.all);
      setState(() {});
    });

  }

  Future<void> _onTap(ApplicationMeta app) async {

    var upiIntentRequest = http.MultipartRequest(
          "POST",
          Uri.parse(payreq.upiIntentRequestUrl),
    );
        

    // upiIntentRequest.fields['return_url'] = "https://uatpgbiz.omniware.in/pay-modal-cdn/test/return.php";
    // upiIntentRequest.fields['order_id'] = random.nextInt(1000000).toString();
    // upiIntentRequest.fields['mode'] = "LIVE";
    // upiIntentRequest.fields['amount'] = "2";
    // upiIntentRequest.fields['currency'] = "INR";
    // upiIntentRequest.fields['description'] = "description";
    // upiIntentRequest.fields['name'] = "Peter parker";
    // upiIntentRequest.fields['email'] = "Peterparker@gmail.com";
    // upiIntentRequest.fields['phone'] = "9836362341";
    // upiIntentRequest.fields['address_line_1'] = "address_line_1";
    // upiIntentRequest.fields['address_line_2'] = "address_line_2";
    // upiIntentRequest.fields['city'] = "city";
    // upiIntentRequest.fields['state'] = "state";
    // upiIntentRequest.fields['country'] = "India";
    // upiIntentRequest.fields['zip_code'] = "560073";
    // upiIntentRequest.fields['udf1'] = "udf1";
    // upiIntentRequest.fields['udf2'] = "udf2";
    // upiIntentRequest.fields['udf3'] = "udf3";
    // upiIntentRequest.fields['udf4'] = "udf4";
    // upiIntentRequest.fields['udf5'] = "udf5";

    upiIntentRequest.fields['return_url'] = payreq.returnUrl;
    upiIntentRequest.fields['order_id'] = payreq.orderId;
    upiIntentRequest.fields['mode'] = payreq.mode;
    upiIntentRequest.fields['amount'] = payreq.amount;
    upiIntentRequest.fields['currency'] = payreq.currency;
    upiIntentRequest.fields['description'] = payreq.description;
    upiIntentRequest.fields['name'] = payreq.name;
    upiIntentRequest.fields['email'] = payreq.email;
    upiIntentRequest.fields['phone'] = payreq.phone;
    upiIntentRequest.fields['address_line_1'] = payreq.addressLine1.toString();
    upiIntentRequest.fields['address_line_2'] = payreq.addressLine2.toString();
    upiIntentRequest.fields['city'] = payreq.city;
    upiIntentRequest.fields['state'] = payreq.state;
    upiIntentRequest.fields['country'] = payreq.country;
    upiIntentRequest.fields['zip_code'] = payreq.zipCode;
    upiIntentRequest.fields['udf1'] = payreq.udf4.toString();
    upiIntentRequest.fields['udf2'] = payreq.udf4.toString();
    upiIntentRequest.fields['udf3'] = payreq.udf4.toString();
    upiIntentRequest.fields['udf4'] = payreq.udf4.toString();
    upiIntentRequest.fields['udf5'] = payreq.udf5.toString();

    
    var streamedResponse = await upiIntentRequest.send();

    var response = await http.Response.fromStream(streamedResponse);
    var jsonResponse = jsonDecode(response.body);
    print(jsonResponse);
    var transactionId = jsonResponse['transaction_id'];
    //jsonResponse['qr_code'].toString().split('?')[1]
    int startIndex = jsonResponse['qr_code'].toString().indexOf('?') + 1;
    var subStr = jsonResponse['qr_code'].toString().substring(startIndex);
    var responseQueryParameters = Uri.splitQueryString(subStr);
    print(responseQueryParameters);

    

    String pa = responseQueryParameters['pa'].toString();
    String pn = responseQueryParameters['pn'].toString();
    String tr = responseQueryParameters['tr'].toString();
    String am = responseQueryParameters['am'].toString();
    /*String mc = responseQueryParameters['mc'] as String;*/



    final upiResponse = await UpiPay.initiateTransaction(
      amount: am,
      app: app.upiApplication,
      receiverName: pn,
      receiverUpiAddress: pa,
      transactionRef: tr,
      transactionNote: 'UPI Payment',
    );

    print(upiResponse);


    var upiResponseRequest = http.MultipartRequest(
          "POST",
          Uri.parse(payreq.upiIntentRequestUrl)
    );

    upiResponseRequest.fields['txnId'] = upiResponse.txnId.toString();
    upiResponseRequest.fields['txnRef'] = upiResponse.txnRef.toString();
    upiResponseRequest.fields['Status'] = upiResponse.status.toString();
    upiResponseRequest.fields['responseCode'] = upiResponse.responseCode.toString();
    upiResponseRequest.fields['transaction_id'] = transactionId.toString();
    upiResponseRequest.fields['get_response'] = 'getResponse';

    var r = await upiResponseRequest.send();
    var intentPaymentResponse = await http.Response.fromStream(r);
    print(jsonDecode(intentPaymentResponse.body));

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: <Widget>[
           Platform.isAndroid ? _androidApps() : _iosApps(),
        ],
      ),
    );
  }


  Widget _androidApps() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (_apps != null) _appsGrid(_apps!.map((e) => e).toList()),
        ],
      ),
    );
  }




  Widget _iosApps() {
    return Container(
      margin: EdgeInsets.only(top: 32, bottom: 32),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 24),
            child: Text(
              'One of these will be invoked automatically by your phone to '
              'make a payment',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12),
            child: Text(
              'Detected Installed Apps',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          if (_apps != null) _discoverableAppsGrid(),
          Container(
            margin: EdgeInsets.only(top: 12, bottom: 12),
            child: Text(
              'Other Supported Apps (Cannot detect)',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          if (_apps != null) _nonDiscoverableAppsGrid(),
        ],
      ),
    );
  }

  GridView _discoverableAppsGrid() {
    List<ApplicationMeta> metaList = [];
    _apps!.forEach((e) {
      if (e.upiApplication.discoveryCustomScheme != null) {
        metaList.add(e);
      }
    });
    return _appsGrid(metaList);
  }

  GridView _nonDiscoverableAppsGrid() {
    List<ApplicationMeta> metaList = [];
    _apps!.forEach((e) {
      if (e.upiApplication.discoveryCustomScheme == null) {
        metaList.add(e);
      }
    });
    return _appsGrid(metaList);
  }
  



  GridView _appsGrid(List<ApplicationMeta> apps) {
    apps.sort((a, b) => a.upiApplication
        .getAppName()
        .toLowerCase()
        .compareTo(b.upiApplication.getAppName().toLowerCase()));
    return GridView.count(
      crossAxisCount: 4,
      shrinkWrap: true,
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      // childAspectRatio: 1.6,
      physics: NeverScrollableScrollPhysics(),
      children: apps
          .map(
            (app) => Material(
              key: ObjectKey(app.upiApplication),
              // color: Colors.grey[200],
              child: InkWell(
                onTap: Platform.isAndroid ? () async => await _onTap(app) : null,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    app.iconImage(48),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      alignment: Alignment.center,
                      child: Text(
                        app.upiApplication.getAppName(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}