import 'package:flutter/material.dart';

import 'payment_util_widgets.dart';
import '../size_config.dart';
import '../payment_req.dart';

class CashOnDeliveryWidget extends StatelessWidget {

    final PaymentReq paymentrequest;
    CashOnDeliveryWidget({required this.paymentrequest});

     @override
     Widget build(BuildContext context) {

       return Container(
         padding: EdgeInsets.all(proportionateSize(12)),
         child: Column(
           children: [
            paymentOptionHeader(
              context,
              Icons.money,
              'Cash on Delivery',
              paymentrequest.amount,
              proportionateSize(32),
            ),
            SizedBox(height: proportionateHeight(20)),
            ElevatedButton(
              child: Text('Pay'),
              onPressed: (){
                
              },
              style: Theme.of(context).elevatedButtonTheme.style,
            ),

           ],

         ),
       );
     
     }

}