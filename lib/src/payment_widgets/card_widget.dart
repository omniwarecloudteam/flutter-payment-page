import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../webview_page.dart';

import 'payment_util_widgets.dart';
import '../size_config.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../payment_req.dart';


class CardWidget extends StatelessWidget {

  final PaymentReq paymentrequest;
  CardWidget({required this.paymentrequest});

     @override
     Widget build(BuildContext context) {
        return Container(
             padding: const EdgeInsets.all(15),
             child: Column(
               children: [

                  paymentOptionHeader(
                    context,
                    Icons.payments_outlined,
                    'Cards',
                    paymentrequest.amount.toString(),
                    proportionateSize(32),
                  ),
                  SizedBox(height: proportionateHeight(20)),
                  placeOutlinedButton(
                    context,
                    'Enter Card Details',
                    Icons.chevron_right,
                    AddNewCardWidget(paymentrequest),
                  ),

              ],
            )
        );

     }
}



class AddNewCardWidget extends StatefulWidget {

  final PaymentReq paymentrequest;
  AddNewCardWidget(this.paymentrequest);
  
  @override
  _AddNewCardWidgetState createState() => _AddNewCardWidgetState();
}


class _AddNewCardWidgetState extends State<AddNewCardWidget> {
  
  final formKey = GlobalKey<FormState>();

  TextEditingController cardNumber = new TextEditingController();
  TextEditingController expiryDate = new TextEditingController();
  TextEditingController cvv = new TextEditingController();
  TextEditingController cardHolderName = new TextEditingController();

  FocusNode expiryDateFocus = new FocusNode();
  FocusNode cvvFocus = new FocusNode();
  FocusNode cardHolderNameFocus = new FocusNode();

  int cvvLength = 3;
  int cardNumberDigits = 16;
  List<int> cardSpacingFormat = [4,9,14];

  bool displayWebview = false;



  void getCardDetails()
  {
    String input =  cardNumber.text.replaceAll(" ","");

    RegExp visaRegex = RegExp('^4');
    RegExp masterCardRegex = RegExp('^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)');
    RegExp rupayRegex = RegExp('^6(?!011)(?:0|52[12])');
    RegExp maestroRegex = RegExp('^(5081|5020|5038|5893|6304|6759|6761|6771|6762|6763)');
    RegExp dinersClubRegex = RegExp('^3(?:0[0-5]|[68][0-9])');
    RegExp amexRegex = RegExp('^3[47]');


    if(visaRegex.hasMatch(input) ||
       masterCardRegex.hasMatch(input) ||
       rupayRegex.hasMatch(input) ||
       maestroRegex.hasMatch(input))
    {
      setState(() {
        cardNumberDigits = 16;
        cvvLength = 3;
        cardSpacingFormat = [4,9,14];
      });
    }
    else if(dinersClubRegex.hasMatch(input))
    {
      setState(() {
        cardNumberDigits = 14;
        cvvLength = 3;
        cardSpacingFormat = [4,11];
      });
    }
    else if(amexRegex.hasMatch(input))
    {
      setState(() {
        cardNumberDigits = 15;
        cvvLength = 4;
        cardSpacingFormat = [4,11];
      });
    }
  }


  @override
  void initState() {
  
    cardNumber.addListener(getCardDetails);
    super.initState();
  }

  @override
  void dispose() {
  
    cardNumber.dispose();
    expiryDate.dispose();
    cvv.dispose();
    cardHolderName.dispose();

    expiryDateFocus.dispose();
    cvvFocus.dispose();
    cardHolderNameFocus.dispose();
    super.dispose();

  }




  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(proportionateSize(25)),
        child: Column(
          children: [
            Text('Enter Card Details',style:Theme.of(context).textTheme.headline1),
            SizedBox(height:proportionateHeight(30)),

            Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
  
                      TextFormField(
                          controller: cardNumber,
                          keyboardType: TextInputType.number,
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_){
                            FocusScope.of(context).requestFocus(expiryDateFocus);
                          },
                          onChanged: (value){},
                          maxLength: cardNumberDigits,
                          maxLengthEnforcement: MaxLengthEnforcement.enforced,
                          inputFormatters: [
                            CardNumberFormatter(cardSpacingFormat),
                            FilteringTextInputFormatter.allow(RegExp('[0-9 ]'))
                          ],
                          decoration: InputDecoration(
                            labelText: 'Card Number',
                            border: OutlineInputBorder(),
                            counterText: "",
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter the card number';
                            }
                            if(!validateCardNumber(cardNumber.text))
                            {
                              return "Please enter a valid card number";
                            }
                            return null;
                          },
              
                      ),
                      SizedBox(height: proportionateHeight(15),),
                      TextFormField(
                        controller: expiryDate,
                        keyboardType: TextInputType.phone,
                        textInputAction: TextInputAction.next,
                        maxLength: 5,
                        maxLengthEnforcement: MaxLengthEnforcement.enforced,
                        inputFormatters: [
                          ExpiryDateFormatter(),
                          FilteringTextInputFormatter.allow(RegExp('[0-9\/]'))
                        ],
                        focusNode: expiryDateFocus,
                        onFieldSubmitted: (_){
                          expiryDateFocus.unfocus();
                          FocusScope.of(context).requestFocus(cvvFocus);
                        },
                        onChanged:(value){},
                        decoration: InputDecoration(
                          labelText: 'Expiry Date',
                          border: OutlineInputBorder(),
                          counterText: "",
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter the expiry date';
                          }
                          if(!validateExpiryDate(expiryDate.text))
                          {
                            return 'Please enter a valid date';
                          }
                          return null;
                        },
                    ),
                  
                  SizedBox(height: proportionateHeight(15),),
                  TextFormField(
                      controller: cvv,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                      maxLength: cvvLength,
                      maxLengthEnforcement: MaxLengthEnforcement.enforced,
                      focusNode: cvvFocus,
                      onFieldSubmitted: (_){
                          cvvFocus.unfocus();
                          FocusScope.of(context).requestFocus(cardHolderNameFocus);
                      },
                      decoration: InputDecoration(
                          labelText: 'CVV',
                          border: OutlineInputBorder(),
                          counterText: "",
                      ),
                      validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter the cvv';
                          }
                          if(!validateCvv(cvv.text, cvvLength))
                          {
                            return 'Invalid number of digits';
                          }
                          return null;
                        },
                    ),
                          
                    
                    SizedBox(height: proportionateHeight(15),),                  
                    TextFormField(
                      style: TextStyle(decoration:TextDecoration.none),
                      controller: cardHolderName,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      focusNode: cardHolderNameFocus,
                      onFieldSubmitted: (_){
                          cardHolderNameFocus.unfocus();
                      },
                      decoration: InputDecoration(
                          labelText: 'Card Holder\'s Name',
                          border: OutlineInputBorder(),
                      ),
                      inputFormatters:[
                        //FilteringTextInputFormatter(, allow: true),
                        FilteringTextInputFormatter.allow(RegExp('[A-Za-z]'))
                      ],
                      validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter the card holder\'s name';
                          }
                          if (!validateCardHolderName(cardHolderName.text)) {
                            return 'Please enter a valid name';
                          }
                          return null;
                        },
                    ),
                    SizedBox(height: proportionateHeight(30)),

                    ElevatedButton(
                      onPressed: () async {
                        
                        if (formKey.currentState!.validate()) 
                        {

                          String cardBin = cardNumber.text.substring(0,6);

                          String url = widget.paymentrequest.cardInfoRequestUrl;
                          
                          var postRequest = http.MultipartRequest(
                                "POST",
                                Uri.parse(url),
                          );
        
                          postRequest.fields['card_bin'] = cardBin;
                          
                          var streamedResponse = await postRequest.send();
                          var response = await http.Response.fromStream(streamedResponse);
                          var jsonResponse = jsonDecode(response.body);

                          String cardBankCode = getBankCodeFromCardInfo(
                                                  jsonResponse['card'].toLowerCase(),
                                                  jsonResponse['type'],
                                                  jsonResponse['level'],
                                                  jsonResponse['country']
                                                );

                          
                          // Navigator.of(context).pushNamed(WebviewPage.routeName,arguments: {
              
                          //     "cards":{"cardNumber":cardNumber.text,
                          //               "expiryDate":expiryDate.text,
                          //               "cvv":cvv.text,
                          //               "cardHolderName":cardHolderName.text,
                          //               "bankCode":cardBankCode},
                          //     "paymentData":widget.paymentrequest,
                          // });
                           
                           Navigator.of(context).pop();
                           
                           Map<String,Map<String,String>> paymentChannelArgs =
                           
                           {"cards":{"cardNumber":cardNumber.text,
                                        "expiryDate":expiryDate.text,
                                        "cvv":cvv.text,
                                        "cardHolderName":cardHolderName.text,
                                        "bankCode":cardBankCode},};

                           Navigator.push(context,
                              MaterialPageRoute(builder: (context) => 
                                WebviewPage(paymentChannelArgs,widget.paymentrequest)),
                           );
  
                        }
                        
                      },
                      child:Text('Pay',),
                      style: Theme.of(context).elevatedButtonTheme.style,
                    )
                  ],
                ),
              ),
            ),


          ],
        ),   
    );
  }


  String getBankCodeFromCardInfo(String? code, String? type, String? level, String? country) 
  {
    
    type = type != null? type:'CREDIT';
    level = level != null? level:null;
    country = country != null? country:null;
    
    var bankCodeList;
    
    var commercialLevels = [
      'BUSINESS',
      'CORPORATE',
      'CORPORATE T&E',
      'PURCHASING',
      'BUSINESS SIGNATURE',
      'WORLD FOR BUSINESS',
      'PLATINUM'
    ];
    
    var prepaidLevels = [
      'PREPAID',
      'PREPAID BUSINESS',
      'PREPAID CORPORATE T&E',
      'PREPAID RELOADABLE',
      'PREPAID PLATINUM',
      'PREPAID NON US GENERAL SP',
      'PREPAID ELECTRON',
      'PREPAID PAYROLL'
    ];
    
    var bankCode = '';
    
    if (country != 'IN') 
    {
        bankCodeList = {
          'CREDIT': {
            'visa': 'VICI',
            'mastercard': 'MACI',
            'amex': 'AMXI',
            'diners': 'DINI'
          },
          'DEBIT': {
            'visa': 'VIDI',
            'mastercard': 'MADI',
            'amex': 'AMXI',
            'diners': 'DINI'
          }
        };

        bankCode = bankCodeList[type] != null &&
                   bankCodeList[type][code] != null ?
                   bankCodeList[type][code] : '';
    }
    
    if(level != null)
    {

      if (commercialLevels.indexOf(level) > -1 && bankCode == '') {
        bankCodeList = {
          'CREDIT': {
            'visa': 'VICC',
            'mastercard': 'MACC'
          }
        };
        bankCode = bankCodeList[type] != null &&
                  bankCodeList[type][code] != null ?
                  bankCodeList[type][code] : '';
      }

    }
    
    /* there is no separate bank codes for prepaid credit or prepaid debit. All are considered as prepaid cards */
    
    if(level != null)
    {
        if (prepaidLevels.indexOf(level) > -1 && bankCode == '') 
        {
          bankCodeList = {
            'CREDIT': {
              'visa': 'VISP',
              'mastercard': 'MASP',
              'rupay': 'RUPP'
            },
            'DEBIT': {
              'visa': 'VISP',
              'mastercard': 'MASP',
              'rupay': 'RUPP'
            }
          };
          bankCode = bankCodeList[type] != null &&
                     bankCodeList[type][code] != null ?
                     bankCodeList[type][code] : '';
        }

    }
    

    if (bankCode == '') {
      bankCodeList = {
        'CREDIT': {
          'visa': 'VISC',
          'mastercard': 'MASC',
          'amex': 'AMXC',
          'diners': 'DINC',
          'maestro': 'MAED',
          'rupay': 'RUPC'
        },
        'DEBIT': {
          'visa': 'VISD',
          'mastercard': 'MASD',
          'rupay': 'RUPD',
          'maestro': 'MAED'
        }
      };
      bankCode = bankCodeList[type] != null &&
                 bankCodeList[type][code] != null ?
                 bankCodeList[type][code] : '';
    }

    return bankCode;
  }



  bool validateCardNumber(String input)
  {
    var nCheck = 0, isEven = false;
    input = input.replaceAll(RegExp('/\D/g'), "");
    for (var n = input.length - 1; n >= 0; n--) {
      var cDigit = input[n],
          nDigit = int.parse(cDigit);

      if (isEven && (nDigit *= 2) > 9) nDigit -= 9;

      nCheck += nDigit;
      isEven = !isEven;
    }
    return (nCheck % 10) == 0;

  }

  bool validateExpiryDate(input)
  {	
    int enteredYear = int.parse("20"+input.substring(3,5));
    int enteredMonth = int.parse(input.substring(0,2));
    DateTime expiryDate = DateTime(enteredYear,enteredMonth);
    DateTime today = DateTime.now();
    int thisYear = today.year;

    return((enteredMonth > 0 && enteredMonth <= 12) && ((enteredYear - thisYear) < 30) && (expiryDate.isAfter(today)));

  }

  bool validateCvv(String input, int cvvLength)
  {
    return input.length == cvvLength;
  }

  bool validateCardHolderName(String input)
  {
    return RegExp('[A-Za-z]').hasMatch(input) && input.length > 2;
  }
}




class CardNumberFormatter extends TextInputFormatter{

  List<int> cardSpacing;
  CardNumberFormatter(this.cardSpacing);

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue,TextEditingValue newValue)
  {

    print(oldValue.text);
    print(newValue.text);

    String value = '';
    bool flag = false;
    bool backspacePressed = false;

    if(oldValue.text.length > newValue.text.length)
    {
      backspacePressed = true;
    }
    else
    {
      for(int i=0;i<cardSpacing.length;i++)
      {
          if(newValue.text.length ==cardSpacing[i])
          {
            value = newValue.text + ' ';
            flag = true;
            break;
          }
      }
    }

    return TextEditingValue(
      text: flag?value:newValue.text,
      selection: TextSelection.collapsed(
        offset:backspacePressed?newValue.text.length
        :flag?value.length:newValue.text.length),
    );
  }

}


class ExpiryDateFormatter extends TextInputFormatter{

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue,TextEditingValue newValue){

    String value = '';
    bool flag = false;
    bool backspacePressed = false;

    print(oldValue.text);
    print(newValue.text);

    if(oldValue.text.length > newValue.text.length)
    {
      backspacePressed = true;
    }
    else
    {
      // if oldValue.text.length > newValue.text.length it signifies that the backspace key has been pressed
      if(newValue.text.length == 2 && newValue.text.length > oldValue.text.length)
      {
        print("4444444");
        value = newValue.text + '/';
        flag = true;
      }
      if(newValue.text.length == 1 && RegExp('[23456789]').hasMatch(newValue.text))
      {
        print("5555555");
        value = '0' + newValue.text + '/';

        flag = true;
      }
      if(newValue.text.length == 2 && double.parse(newValue.text) > 12 && !newValue.text.contains('/'))
      {
        print("66666666");
        value = oldValue.text;
        flag = true;
      }

    }
  
    return TextEditingValue(
      text: flag?value:newValue.text,
      selection: TextSelection.collapsed(
        offset: backspacePressed?newValue.text.length
        :flag?value.length:newValue.text.length),
    );

  }

} 

