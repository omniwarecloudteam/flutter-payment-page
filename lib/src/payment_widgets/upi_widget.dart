import 'package:flutter/material.dart';

import 'upi_intent.dart';
import 'payment_util_widgets.dart';
import '../size_config.dart';
import '../webview_page.dart';

import '../payment_req.dart';


class UpiWidget extends StatelessWidget {

  final PaymentReq paymentrequest;
  UpiWidget({required this.paymentrequest});

     @override
     Widget build(BuildContext context) {
       
        return Container(
            padding: const EdgeInsets.all(15),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  paymentOptionHeader(
                    context,
                    Icons.double_arrow,
                    'UPI',
                    paymentrequest.amount.toString(),
                    proportionateSize(34)
                  ),
                  SizedBox(height: proportionateHeight(10)),
                  UpiIntent(paymentrequest),
                  SizedBox(height: proportionateHeight(10)),
                  placeOutlinedButton(
                    context,
                    'Enter UPI ID',
                    Icons.chevron_right,
                    AddUpiWidget(paymentrequest),
                    proportionateHeight(230),
                  ),
                  
                ],
              ),
        );
     
     }
}


class AddUpiWidget extends StatefulWidget {

  final PaymentReq paymentrequest;
  AddUpiWidget(this.paymentrequest);

  @override
  _AddUpiWidgetState createState() => _AddUpiWidgetState();
}

class _AddUpiWidgetState extends State<AddUpiWidget> {

  final formKey = GlobalKey<FormState>();

  TextEditingController payerVirtualAddress = new TextEditingController();

  bool displayWebview = false;

  @override
  void dispose() {
    payerVirtualAddress.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Text('Enter UPI ID',style:Theme.of(context).textTheme.headline1),
          SizedBox(height: proportionateHeight(20),),
          Form(
            key: formKey,
            child: Padding(
                  padding: EdgeInsets.only(bottom:MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextFormField(
                          controller: payerVirtualAddress,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                            labelText: 'VPA',
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your VPA';
                            }

                            final validVpa = RegExp('[A-Za-z0-9.-_]+@[A-Za-z]+');
                            if(!validVpa.hasMatch(value))
                            {
                              return 'Enter a valid VPA';
                            }
                            return null;
                          },
        
                      ),
                      SizedBox(height: proportionateHeight(30)),
                      ElevatedButton(
                        onPressed: () async {
              
                          if (formKey.currentState!.validate()) 
                          {

                            // Navigator.of(context).pushReplacementNamed(WebviewPage.routeName,arguments: {
                            //     "upi":{"bankCode":"UPIU",
                            //             "payerVirtualAddress":payerVirtualAddress.text},
                            //     "PaymentData":widget.paymentrequest,
                            // });
                           
                           Map<String,Map<String,String>> paymentChannelArgs =
                           
                           {"upi":{"bankCode":"UPIU",
                                  "payerVirtualAddress":payerVirtualAddress.text}};

                           Navigator.of(context).pop();
                           Navigator.push(context,
                              MaterialPageRoute(builder: (context) => 
                                WebviewPage(paymentChannelArgs,widget.paymentrequest)),
                           );

                          }
          
                        },       
                        child:Text('Pay',),
                        style: Theme.of(context).elevatedButtonTheme.style,
                      ),
                    ],
                  ),
                ),
                
          ),
        ],
      ),
 
      
    );
  }
}