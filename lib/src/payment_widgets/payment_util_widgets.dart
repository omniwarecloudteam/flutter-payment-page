import 'package:flutter/material.dart';

import '../size_config.dart';


Widget paymentOptionHeader(BuildContext ctx,IconData icon,String title,String amount,[double? iconSize]){

    return Row(
            children: [
              Icon(
                icon,
                size: iconSize == null? proportionateSize(30):iconSize,
                color: Color(0xff4a4a4a),
              ),
              SizedBox(width: proportionateWidth(10)),
              Expanded(
                child: Text(title,style:Theme.of(ctx).textTheme.headline1),
              ),
              Text('\u{20B9} '+amount,style:Theme.of(ctx).textTheme.headline1),
              
            ],
          );
} 


Widget placeOutlinedButton(BuildContext ctx,
                           String buttonText,
                           IconData icon,
                           Widget modalWidget,
                           [double? height])
{
    return OutlinedButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(buttonText),
                  SizedBox(width: proportionateSize(10),),
                  Icon(icon,size: proportionateSize(20),),
                ],
              ),
              onPressed: (){
                  showModalBottomSheet(context: ctx,
                  isScrollControlled: true,
                   builder: (_){
                      return SingleChildScrollView(
                        child: modalWidget,
                      );
                  },);
              },
                style: Theme.of(ctx).outlinedButtonTheme.style,  
            );

}