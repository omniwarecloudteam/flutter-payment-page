import 'package:flutter/material.dart';

import 'payment_util_widgets.dart';
import '../size_config.dart';
import '../app_data/wallet.dart';
import '../webview_page.dart';
import '../payment_req.dart';

class WalletWidget extends StatelessWidget {

    final PaymentReq paymentrequest;
    final List<String> bankCodesList;

    WalletWidget({
      required this.paymentrequest,
      required this.bankCodesList,
    });
  
     @override
     Widget build(BuildContext context) {

        Widget walletTile(BuildContext context, Map<String,String> walletTile)
        {
          return GestureDetector(
            onTap: (){

              // Navigator.of(context).pushNamed('/WebviewPage',
              //   arguments: {
              //     "wallet":{"bankCode":walletTile['bankCode'].toString()}                  
              //   }
              // );
              Map<String,Map<String,String>>? bankCodeArgs = 
              {
                  "wallet":{"bankCode":walletTile['bankCode'].toString()}                  
              };


              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => 
                    WebviewPage(bankCodeArgs,paymentrequest)),
              );

            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal:13),
              margin: EdgeInsets.all(7),
              child: Column(
                children: [
                  Image.asset('images/'+walletTile['image'].toString()+'.png',
                    height: proportionateHeight(double.parse(walletTile['height'].toString())),
                    width: proportionateWidth(double.parse(walletTile['width'].toString())),
                  ),
                  SizedBox(height: proportionateHeight(5),),
                  Text(
                    walletTile['title'].toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption,
                  ),
              ],),
            
            ),
            
          ); 

        }



        return Container(
            padding: const EdgeInsets.all(15),
              child:Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  paymentOptionHeader(
                    context,
                    Icons.account_balance_wallet_sharp,
                    'Wallet',
                    paymentrequest.amount.toString()
                  ),
                  SizedBox(height: proportionateHeight(15)),
                  Container(
                    height: proportionateHeight(85),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children:[
                        
                        ...walletTiles.map(
                          (tile) =>walletTile(context,tile)
                        ).toList(),

                      ],
                    ),
                  ),

                  SizedBox(height: proportionateHeight(15)),

                  placeOutlinedButton(
                    context,
                    'More Wallets',
                    Icons.chevron_right,
                    ChooseWalletWidget(paymentrequest,bankCodesList),
                  ),
                  
                ],
              ),
             
        );
     
     }

}


class ChooseWalletWidget extends StatefulWidget {

  final PaymentReq paymentrequest;
  final List<String> bankCodesList;
  ChooseWalletWidget(this.paymentrequest,this.bankCodesList);

  @override
  _ChooseWalletWidgetState createState() => _ChooseWalletWidgetState();

}


class _ChooseWalletWidgetState extends State<ChooseWalletWidget> {

 int selectedIndex = -1;
 bool selected = false;
 Map<String,Map<String,String>>? bankCodeArgs;

 Widget walletListTile(BuildContext context, String bankCode, int index)
 {
   return GestureDetector(
          onTap: (){

            bankCodeArgs = {
                "netbanking":{"bankCode":bankCode}      
            };

            setState(() {
              selectedIndex = index;  
            });

          },
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(8,8,8,8),
                color: selectedIndex == index? Color(0xffeeeeee):null,
                child:Row(
                  children: [
                    Icon(
                      Icons.account_balance_wallet_sharp,
                      size: proportionateSize(35),
                      color: Color(0xffababab),
                    ),
                    SizedBox(width: proportionateWidth(30),),
                    Expanded(
                      child: Text(
                        walletOptionsMapper[bankCode.toUpperCase()].toString().toUpperCase(),
                        style:Theme.of(context).textTheme.subtitle1,
                      ),
                    ),
                  ],
                ),
                  
              ),
              Container(
                margin: EdgeInsets.fromLTRB(60,0,0,0),
                child: Divider(),
              ),
            ],
          )
        );
 }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(proportionateSize(25)),
      child: Column(
        children: [
          Text('Select a Wallet',style: Theme.of(context).textTheme.headline1),
          SizedBox(height: proportionateHeight(15)),
          Container(
            margin: EdgeInsets.all(proportionateSize(5)),
            height: proportionateHeight(300),
            child: ListView.builder(
              itemCount: widget.bankCodesList.length,
              itemBuilder: (context, index){
                /*...bankList.map(
                    (listTile) => netbankingListTile(context,listTile),
                  ).toList(),*/
                return walletListTile(context, widget.bankCodesList[index], index);
              },  
                
            ),
                 
          ),
          SizedBox(height: proportionateHeight(15)),
          ElevatedButton(
            onPressed: (){

              if(bankCodeArgs != null)
              {
                // Navigator.of(context).pushNamed(WebviewPage.routeName,
                //   arguments: bankCodeArgs           
                // );
                Navigator.of(context).pop();

                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => 
                    WebviewPage(bankCodeArgs,widget.paymentrequest)),
                );


              }

            },
            child:Text('Pay'),
            style: Theme.of(context).elevatedButtonTheme.style,
          ),

        ],
      ),
      
    );
  }
}