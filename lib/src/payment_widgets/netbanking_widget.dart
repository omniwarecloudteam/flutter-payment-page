import 'package:flutter/material.dart';

import '../webview_page.dart';
import '../app_data/netbanking.dart';
import 'payment_util_widgets.dart';
import '../size_config.dart';
import '../payment_req.dart';


class NetbankingWidget extends StatelessWidget {

    final PaymentReq paymentrequest;
    final List<String> bankCodesList;

    NetbankingWidget({
      required this.paymentrequest,
      required this.bankCodesList,
    });

     @override
     Widget build(BuildContext context) {

        Widget netbankingTile(BuildContext ctx, Map<String,String> bankTile)
        {
          return GestureDetector(

            onTap: (){

              // Navigator.of(ctx).pushNamed('/WebviewPage',
              //   arguments: {
              //     "netbanking":{"bankCode":bankTile['bankCode'].toString()}                  
              //   }
              // );

              Map<String,Map<String,String>>? paymentChannelArgs = 
              {
                  "netbanking":{"bankCode":bankTile['bankCode'].toString()}                  
              };

              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => 
                    WebviewPage(paymentChannelArgs,paymentrequest)),
              );

            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal:13),
              margin: EdgeInsets.all(7),
              child: Column(
                children: [
                  Image.asset('images/'+bankTile['image'].toString()+'.png',
                    height: proportionateHeight(double.parse(bankTile['height'].toString())),
                    width: proportionateWidth(double.parse(bankTile['width'].toString())),
                  ),
                  SizedBox(height: proportionateHeight(5),),
                  Text(
                    bankTile['title'].toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption,
                  ),
              ],),
            
            ),
            
          ); 

        }



        return Container(
            padding: EdgeInsets.all(proportionateSize(15)),
              child:Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  paymentOptionHeader(
                    context,
                    Icons.account_balance_sharp,
                    'Netbanking',
                    paymentrequest.amount.toString()
                  ),
                  SizedBox(height: proportionateHeight(15)),
                  Container(
                    height: proportionateHeight(85),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children:[
                        
                        ...bankTiles.map(
                          (tile) =>netbankingTile(context,tile)
                        ).toList(),
                          
                        
                      ],
                    ),
                  ),
                  SizedBox(height: proportionateHeight(15)),
                  placeOutlinedButton(
                    context,
                    'More Banks',
                    Icons.chevron_right,
                    ChooseBankWidget(paymentrequest,bankCodesList),
                  ),
                  
                ],
              ),
             
        );
     
     }

}





class ChooseBankWidget extends StatefulWidget {

  final PaymentReq paymentrequest;
  final List<String> bankCodesList;
  ChooseBankWidget(this.paymentrequest,this.bankCodesList);

  @override
  _ChooseBankWidgetState createState() => _ChooseBankWidgetState();
}

class _ChooseBankWidgetState extends State<ChooseBankWidget> {

 int selectedIndex = -1;
 bool selected = false;
 Map<String,Map<String,String>>? paymentChannelArgs;

 


 Widget netbankingListTile(BuildContext ctx, String bankCode, int index)
 {

   for(int i = 0; i < widget.bankCodesList.length; i++)
  {
    

  }



   return GestureDetector(
          onTap: (){

            paymentChannelArgs = {
              "netbanking":{"bankCode":bankCode}                  
            };

            setState(() {
              selectedIndex = index;  
            });
            

          },
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(8,8,8,8),
                color: selectedIndex == index? Color(0xffeeeeee):null,
                child:Row(
                  children: [
                    Icon(
                      Icons.account_balance_sharp,
                      size: proportionateSize(35),
                      color: Color(0xffababab),
                    ),
                    SizedBox(width: proportionateWidth(30),),
                    Expanded(
                      child: Text(
                        netbankingOptionsMapper[bankCode.toUpperCase()].toString().toUpperCase(),
                        style:Theme.of(context).textTheme.subtitle1
                      ),
                    ),
                  ],
                ),
                  
              ),
              Container(
                margin: EdgeInsets.fromLTRB(60,0,0,0),
                child: Divider(),
              ),
            ],
          )
        );
 }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(proportionateSize(25)),
      child: Column(
        children: [
          Text('Select a bank',style: Theme.of(context).textTheme.headline1),
          SizedBox(height: proportionateHeight(15)),
          Container(
            margin: EdgeInsets.all(proportionateSize(5)),
            height: proportionateHeight(300),
            child: ListView.builder(
              itemCount: widget.bankCodesList.length,
              itemBuilder: (context, index){
                /*...bankList.map(
                    (listTile) => netbankingListTile(context,listTile),
                  ).toList(),*/
                return netbankingListTile(context, widget.bankCodesList[index], index);
              },  
                
            ),
                 
          ),
          SizedBox(height: proportionateHeight(15)),
          ElevatedButton(
            onPressed: (){

              if(paymentChannelArgs != null)
              {

                // Navigator.of(context).pushNamed(WebviewPage.routeName,
                //   arguments: paymentChannelArgs           
                // );
                Navigator.of(context).pop();

                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => 
                    WebviewPage(paymentChannelArgs,widget.paymentrequest)),
                );
              }

            },
            child:Text('Pay'),
            style: Theme.of(context).elevatedButtonTheme.style,
          ),

        ],
      ),
      
    );
  }
}