import 'package:flutter/material.dart';

import 'src/payment_page.dart';
import 'src/payment_req.dart';

export 'src/payment_page.dart';
export 'src/payment_req.dart';



  
void displayPaymentPage
({
   required BuildContext context,
   required PaymentReq paymentRequest,
   required List<String> netbankingBankCodes,
   required List<String> walletBankCodes,
})
{
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => 
          PaymentPage(
            paymentRequest:paymentRequest,
            netbankingCodesList: netbankingBankCodes,
            walletCodesList: walletBankCodes
          )
    ),
  );
}
